import Api from '../Api'

export default {
  login (payload) {
    const request = `{ "data": ${JSON.stringify(payload)} }`;
    console.log('login request \n', request);
    return Api('application/json').post(`/login`, request).then(response => { return response.data; });
  },

  verifyToken (payload) {
    const request = `{ "data": ${JSON.stringify(payload)} }`;
    console.log('verifyToken request \n', request);
    return Api('application/json').post(`/verifyToken`, request).then(response => { return response.data; });  
  }
}
