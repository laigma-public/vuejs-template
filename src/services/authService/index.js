import vueRouter from 'vue-router'
import authService from '@/services/authService/authService'
import cryptoService from '@/services/cryptoService/cryptoService'

const ACCESS_TOKEN_KEY = 'access_token';

const router = new vueRouter({
	mode: 'history',
});

export async function login (credentials) {
	// Encripta credenciales
	const hash = cryptoService.encrypt(JSON.stringify(credentials));
  try {
    const res = await authService.login(hash);
    // console.log('authService.login response \n', res);
    if (res.status == 'S') {
      // Guarda en las cookies
      localStorage.setItem(ACCESS_TOKEN_KEY, JSON.stringify({
        access_token: res,
        username_key: credentials.user
      }));
      router.go('/');
    } else {
      clearSessionStorage();
      return res;
    }
  } catch {
    clearSessionStorage();  
  }
}

export function logout () {
	clearSessionStorage();
	router.go('/');
}

export async function requireAuth (to, from, next) {
	const isLogged = await isLoggedIn();
	if (!isLogged) {
		next({
			path: '/login'
		});
	} else {
		next();
	}
}

export async function requireGuest (to, from, next) {
	const isLogged = await isLoggedIn();
	if (isLogged) {
		next({
			path: '/',
		});
	} else {
		next();
	}
}

function clearSessionStorage () {
	localStorage.removeItem(ACCESS_TOKEN_KEY);
}

export async function isLoggedIn () {
  const tokenData = JSON.parse(localStorage.getItem(ACCESS_TOKEN_KEY));  
  if (tokenData) {
    const res = await authService.verifyToken(tokenData.access_token);
    // console.log('authService.verifyToken response \n', res);
    return res.authorized;
  } else {
    clearSessionStorage();
    return false;
  }	
}