import Api from '../Api'

export default {
  getExample (token) {
    return Api(token).get('/getExample').then(response => { return response.data; });
  }
};