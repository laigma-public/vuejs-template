import axios from 'axios'
import config from '@/config'

const baseURL = `${config.services.apiProtocol}://${config.services.apiHost}:${config.services.apiPort}`;

export default (token) => {
	return axios.create({
		baseURL: baseURL,
		headers: {
      'access-token': token,
			'Access-Control-Allow-Origin': '*',			
			'Content-Type': 'application/json'
		}
	})
}