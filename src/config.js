export default {
	services: {
		apiHost: process.env.API_HOST,
		apiPort: process.env.API_PORT,
		apiProtocol: process.env.API_PROTOCOL,
		staticServer: process.env.STATIC_SERVER
	},
  secrets: {
    key: process.env.APP_SECRET
  }
};
