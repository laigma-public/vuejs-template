import backService from '@/services/pgService/pgService'
import store from './index'

export default {
  async loadUserData (login) {
    const res = await backService.getUserData(login);
    console.log('getUserData response \n', res);
    store.commit('saveUserData', res[0]);
  },

  async loadWeight (userLogin) {
    const res = await backService.getUserWeightData(userLogin);
    console.log('getUserWeightData response \n', res);
    store.commit('saveWeightData', res);
  }
}