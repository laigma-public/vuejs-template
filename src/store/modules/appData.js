// initial state
const state = {
  userData: [],
  userWeight: []
}

const mutations = {
  saveUserData (state, data) {     
    state.userData = data
  },
  saveWeightData (state, data) {     
    state.userWeight = data
  }
}

const getters = {
  userLogged: state => state.userData,
  weightData: state => state.userWeight
}

export default {
  state,
  mutations,
  getters
}