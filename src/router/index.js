import Vue from 'vue'
import VueRouter from 'vue-router'
import { requireAuth, requireGuest } from '@/services/authService'

Vue.use(VueRouter)

const authPages = {
  path: '/',
  component: () => import('@/views/Auth/AuthLayout.vue'),
  name: 'Authentication',
  beforeEnter: requireGuest,
  children: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('@/views/Auth/Login.vue')
    }
  ]
};

const routes = [
  {
    path: '/',
    component: () => import('@/views/Layout/DashboardLayout.vue'),
    beforeEnter: requireAuth,
    children: [
      {
        path: '/',
        name: 'Home',
        component: () => import('@/views/Home.vue')
      }
    ]
  },
  authPages
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
