# build stage
FROM node:14.16.1-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN apk add --update python make g++\ && rm -rf /var/cache/apk/*
RUN npm install
COPY . .
# # RUN rm -r /app/dist
RUN npm run build

# production stage
FROM nginx:1.13.12-alpine as production-stage
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]